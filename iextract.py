import plistlib
import sqlite3
import shutil
import sys
import os

if len(sys.argv) < 3:
    print(f"Usage: python3 {sys.argv[0]} inputfolder outputfolder [covert]")
    print(f"\nOptional Arguments:\n\t convert\t Convert Apple's HEIC format to JPEG\n")
    exit()

input_folder = sys.argv[1]
output_folder = sys.argv[2]
convert = None

if len(sys.argv) == 4:
    convert = True

db_path = f"{input_folder}/Manifest.db"
if not os.path.exists(db_path):
    print("Error: Could not find Manifest.db, are you sure this is a backup?")
    exit()

connection = sqlite3.connect(db_path)
cur = connection.cursor()
    
def parse(fileid):
    fileblob = cur.execute(f"select file from files where fileID='{fileid}'").fetchall()[0][0]
    objects = plistlib.loads(fileblob)['$objects']
    file_path = os.path.join(input_folder, fileid[:2], fileid)
    path_exists = os.path.exists(file_path)
    try:
        metadata = plistlib.loads(objects[3])
        file_name = metadata['com.apple.assetsd.originalFilename'].decode('utf-8')
        file_year = plistlib.loads(metadata['com.apple.assetsd.addedDate']).year
    except Exception:
        file_name = None
        file_year = None
    finally:
        if file_name:
            print(f"{file_path} ({file_name})")
        return {'path': file_path,
                'year': str(file_year),
                'name': file_name,
                'exists': path_exists}
                
def main():

    print(f"[*] Starting search for media files...")

    files = []
    for file_data in cur.execute("select * from files where domain=\"CameraRollDomain\"").fetchall():
        file = parse(file_data[0])
        if file['exists'] and file['name']: # Ignore thumbnails
            files.append(file)

    print(f"[+] Found a total of {len(files)} media files, writing to disk...")
    
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    
    years = []
    for file in files:
        year = file['year']
        if year not in years:
            os.mkdir(os.path.join(output_folder, year))
            years.append(year)
        new_path = f"{output_folder}/{year}/{file['name']}"
        print(f"[*] Extracting {new_path} ", end="", flush=True)
        shutil.copyfile(file['path'], new_path)
        
        if convert and ".HEIC" in file['name']:
            print('(Converting)', end="", flush=True)
            new_new_path = new_path.replace('.HEIC', '.JPEG')
            os.system(f'heif-convert {new_path} {new_new_path} 2>&1 >/dev/null && rm {new_path}')
            os.system(f'convert {new_new_path} -rotate 270 {new_new_path} 2>&1 >/dev/null')
            
        print("(Done)")
        
    print(f"[+] Finished!")
    
    
main()