# iextract

Extracts photos and video files from an iPhone backup. 

~~~
Usage: python3 iextract.py inputfolder outputfolder [covert]

Optional Arguments:
	 convert	 Convert Apple's HEIC format to JPEG
~~~

Inlcudes the ability to automatically convert Apple's HEIC format to a more standard format like JPEG. Using this feature requires additional packages to be installed.

* libheif-examples (`heif-convert`)
* imagemagick-6.q16 (`convert`)

## Example Usage

~~~
$ python3 iextract.py backup/00000000-0000000000000000/ media convert
[*] Starting search for media files...
backup/00000000-0000000000000000/40/7782391677c36f0f0e77363c7ef182e4e75e7669 (IMG_9999.HEIC)
[+] Found a total of 1 media files, writing to disk...
[*] Extracting media/2022/IMG_9999.HEIC (Converting)(Done)
[+] Finished!
~~~

~~~
media
└── 2022
    └── IMG_9999.JPEG
~~~